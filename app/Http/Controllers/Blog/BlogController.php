<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\BlogPost;
use Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = BlogPost::join('categories','blog_posts.categoris_id','=','categories.id')->select('blog_posts.*','categories.cat_name')->get();

        
        return view('blog.posts.show',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $category = Category::all();
          
         return view('blog.posts.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
        'categoris_id' => 'required',
        'title' => 'required|max:255|min:3',
        'description' => 'required',
        'images' => 'required',
    ]);


        $std=new BlogPost;

        if($request->hasfile('images'))
        {

            $image=$request->file('images');
            $file_name=time().'.'.$images->getClientOriginalExtension();
           

            $image_resize= Image::make($images->getRealPath());
            $image_resize->resize(1000,600);

            

            $image_resize->save('images/'.$file_name);

           
             $std->image=$file_name;
            
        }
        $std->title=$request->title;
        $std->description=$request->description;
        $std->categoris_id=$request->categoris_id;
        $std->save();

        
        return back()->with('success','Data Insert Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
