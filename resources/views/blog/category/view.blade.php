@extends('FrontEnd.master')
@section('title','categories view')
@section('content')
<br>
<br>
<br>
 	<div class="container">
 		<table border="1"> 
 			<tr>
 				<th>SI</th>
 				<th>Category Name</th>
 				<th>Category Code</th>
 				<th>Action</th>
 			</tr>
 			@foreach($category as $key=> $data)
 			<tr>
 				<th>{{++$key}}</th>
 				<th>{{$data->cat_name}}</th>
 				<th>{{$data->cat_code}}</th>
 				<th>Edit | Delete</th>
 			</tr>
 			@endforeach
 			
 		</table>
 		{{$category->links()}}
 		
 		
 	</div>
 	<br>
 	<br>
 	<br>


@endsection