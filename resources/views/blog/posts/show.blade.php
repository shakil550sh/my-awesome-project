<div class="container" align="center">
    <table border="1">
      <tr>
        <th>Si</th>
        <th>category name</th>
        <th>Post Title</th>
        <th>Post Description</th>
        <th>Post Images</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr> 
      @foreach($data as $key=>$cat)
      <tr>
        <td>{{++$key}}</td>
        <td>{{$cat->cat_name}}</td>
        <td>{{$cat->title}}</td>
        <td>{{substr($cat->description,0,60)}}...</td>

        <td>
          <img src="{{asset('images/'.$cat->image)}}" width="150">
        </td>

        <td>
            <a href="/posts/{{$cat->id}}/edit">Edit</a>
         </td>
         <td>
           {!!Form::open(['url' => '/posts/'.$cat->id,'method'=>'Delete'])!!}
           <button type="submit" onclick="return confirm('Are You Sure???')">Delete</button>
           {!! Form::close() !!}
         </td>
      </tr>
      @endforeach
    </table>