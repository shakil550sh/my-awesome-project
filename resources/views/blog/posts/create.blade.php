@extends('FrontEnd.master')
@section('title','post')
@section('content')
<br>
<br>
<br>
<br>

	<form class="form-horizontal" action="/posts" method="POST">
    {{csrf_field()}}

		<div class="form-group">
      <label class="control-label col-sm-2" for="email">Select categories:</label>
        <div class="col-sm-10">
            <select name="categoris_id" class="form-control">
      	    <option value="">select a category</option>
          @foreach($category as $cat)
      	   <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
          @endforeach
        </select>
      </div>
    </div>
   

      <div class="form-group">
        <label class="control-label col-sm-2" for="title">title</label>
        <div class="col-sm-10">
          <input type="text" name="title" class="form-control" id="title" placeholder="Enter title">
        </div>
      </div> 

      <div class="form-group">
        <label class="control-label col-sm-2" for="image">Upload a photo</label>
        <div class="col-sm-10">
          <input type="file" class="form-control" id="image" name="images">
        </div>
      </div>

       <div class="form-group">
        <label class="control-label col-sm-2" for="description">description:</label>
        <div class="col-sm-10">
        	<textarea type="text" class="form-control" name="description" id="descrip" placeholder="description"></textarea>
          
        </div>
      </div> 
 
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Posts</button>
        </div>
      </div>
  </form>





    <br>
    <br>
    <br>
</div>

@endsection