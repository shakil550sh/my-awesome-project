<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::resource('/','HomeController');

Route::get('/posts/create','Blog\BlogController@create');
Route::post('/posts','Blog\BlogController@store');
Route::get('/posts','Blog\BlogController@index');


// Route::resource('/categories','Category\CategoryController');

Route::get('/categories/create','Category\CategoryController@create');
Route::post('/categories','Category\CategoryController@store');
Route::get('/categories','Category\CategoryController@index');
// Route::get('/show',function(){
// 	return view('blog.posts.show');
// });

